<!DOCTYPE html>
<html lang="en">

<head>
    <title>iPhone</title>    
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="iphone_style.css">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter&family=Roboto&display=swap" rel="stylesheet">

        <script>

            window.onload = function() {
                document.getElementById('storage').value = "128GB SSD storage";
                document.getElementById('memory').value = "--";
                document.getElementById('serialNumber').value = "12345678910";
                document.getElementById('subTotal').value = "$999.00";
                document.getElementById('totalShip').value = "999";
            };

            function submitForm(memory, storage, serialNumber, subTotal, totalShip) {
                // set default of storage
                if (storage !== '') {
                    document.getElementById('storage').value = storage;
                } 
                if(memory !== '') {
                    document.getElementById('memory').value = memory;
                }
                if (subTotal !== '') {
                    document.getElementById('subTotal').value = subTotal;
                } 
                if (totalShip !== '') {
                    document.getElementById('totalShip').value = totalShip;
                }
                document.getElementById('dataForm').submit();
            }
        </script> 
</head>

<body id="noscroll">
 
    <!-- menu bar -->
    <div class="menu_bg">
        <nav> 
            <!-- destop size -->
            <ul class="destop">
                <li>
                    <a href="mainpage.html" class="apple_logo_white"></a>
                </li>
                <li>
                    <a href="mac.php">Mac</a>
                </li>
                <li>
                    <a style="text-decoration: underline;" href="iphone.php">iPhone</a>
                </li>
                <li>
                    <a href="iwatch.php">iWatch</a>
                </li>
                <li>
                    <a href="ipad.php">iPad</a>
                </li>
                <li>
                    <a href="airpods.php">Airpods</a>
                </li>
                <li>
                    <a href="search.html" class="search_logo"></a>
                </li>
                <li>
                    <a href="ship.php" class="ship_logo"></a>
                </li>
            </ul>
            <!-- destop size -->
        </nav>
    </div>
    <!-- menu bar -->

    <!-- body -->
    <div class="iphone_bg">
        <nav class="text">
            <h1 style="font-size: 30px;">iPhone</h1>
        </nav> 
        <nav class="learnmore"> 
            <img id="click" style="width: 80px; height: 15px; float: right;" src="images/learn_more.png">
            <div id="showdialog" class="dialog_bg">
                <div class="dialog_shape">
                    <span class="close">
                        <img style="width: 50px; height: 50px;" src="images/close.png">
                    </span>
                    <h1 style="margin-top: 60px; margin-left: 20px; margin-right: 40px; margin-bottom: 50px; color: white;" class="dialog_title slideInRight animateTime">Enter A17 Pro.</h1>
                    <h1 style="margin-left: 20px; margin-right: 40px; margin-bottom: 20px; color: white;" class="dialog_title_detail1_2_3_4">Game-changing chip. Groundbreaking performance.</h1>
                    <img style="width: 95%; margin-left: 20px; margin-right: 20px; margin-bottom: 50px;" src="images/iphone_game.png" class="img show">
                    <h1 style="margin-left: 20px; margin-right: 40px; margin-bottom: 20px; color: white;" class="dialog_title_detail1_2_3_4">So strong. So light. So Pro.</h1>
                    <div style="padding-bottom: 50px;">
                        <video id="dialog_video1" style="border-radius: 20px; width: 95%; margin-left: 20px; margin-right: 20px;" loop>
                            <source src="videos/iphone_shape.mp4" type="video/mp4" />
                        </video>
                    </div>
                    <h1 style="margin-left: 20px; margin-right: 40px; margin-bottom: 20px; color: white;" class="dialog_title_detail1_2_3_4">iPhone 14 Pro Max has the longest optical zoom in iPhone ever. Far out.</h1>
                    <div style="padding-bottom: 50px;">
                        <video id="dialog_video2" style="border-radius: 20px; width: 95%; margin-left: 20px; margin-right: 20px;" loop>
                            <source src="videos/iphone_camera.mp4" type="video/mp4" />
                        </video>
                    </div>
                    <h1 style="margin-left: 20px; margin-right: 40px; margin-bottom: 20px; color: white;" class="dialog_title_detail1_2_3_4">All-new Action button.What will yours do?</h1>
                    <div style="padding-bottom: 20px;">
                        <video id="dialog_video3" style="border-radius: 20px; width: 95%; margin-left: 20px; margin-right: 20px;" loop>
                            <source src="videos/side_button.mp4" type="video/mp4" />
                        </video>
                    </div>
                    <h1 style="margin-left: 20px; margin-right: 40px; margin-bottom: 20px; color: white;" class="dialog_title_detail1_2_3_4">Swap numbers with NameDrop</h1>
                    <div style="padding-bottom: 20px;">
                        <video id="dialog_video4" style="border-radius: 20px; width: 95%; margin-left: 20px; margin-right: 20px;" loop>
                            <source src="videos/airdrop.mp4" type="video/mp4" />
                        </video>
                    </div>
                </div>
            </div>
        </nav>
    </div>
    <div class="row">
        <div class="columnvideo">
            <img src="images/iphone.png" class="iphone">
            <div style="padding-bottom: 50px;">
                <video id="main_video" class="video" autoplay loop>
                    <source src="videos/iphone.mp4" type="video/mp4" />
                </video>
            </div>
        </div> 
        <div class="columnchoose">
            <h1 style="font-size: 30px; margin-bottom: 50px;">Buy iPhone 14 Pro</h1>
            <h1 style="font-size: 15px; ">Storage</h1>
            <div onclick="GB128(100); submitForm('', '128GB SSD storage', '', totalPriceiPhoneSubTotal, totalPriceiPhone)" id="GB128" class="GB128_bg">
                <div class="GB128_bg_text">
                    <h1 style="font-size: 15px;">128GB SSD storage</h1>
                </div>
                <div class="GB128_bg_price">
                    <h1 id="GB128_price" style="font-size: 15px;"></h1>
                </div>
            </div>
            <div onclick="GB256(100); submitForm('', '256GB SSD storage', '', totalPriceiPhoneSubTotal, totalPriceiPhone)" id="GB256" class="GB256_bg">
                <div class="GB256_bg_text">
                    <h1 style="font-size: 15px;">256GB SSD storage</h1>
                </div>
                <div class="GB256_bg_price">
                    <h1 id="GB256_price" style="font-size: 15px;">+ $100.00</h1>
                </div>
            </div>
            <div class="line"></div>
            <img style="width: 85%; " src="images/box_things.png">
            <nav class="total">
                <h1 style="font-size: 30px;" >Total :&nbsp;</h1>
                <h1 id="total" style="font-size: 30px;" >$999.00</h1>
            </nav>
            <nav class="ship">
                <form id="shipForm" action="ship.php" method="post">
                    <!-- Hidden input fields to store selected memory and storage options -->
                    <input type="hidden" name="storage" id="storage">
                    <input type="hidden" name="memory" id="memory">
                    <input type="hidden" name="serialNumber" id="serialNumber">
                    <input type="hidden" name="model" id="model" value="iPhone">
                    <input type="hidden" name="subTotal" id="subTotal">
                    <input type="hidden" name="totalShip" id="totalShip">
                    <input type="image" style="width: 10%; border-radius: 40px; margin-right: 20px; margin-bottom: 50px; float: right;" src="images/cart.png" alt="Submit">
                </form>
            </nav>
        </div>
    </div> 
    <div class="end">
        <center>
            <h3 style="padding-top: 20px;" >Preparing by Group 1</h3>
            <div style="padding-top: 20px;" >Srun kheangsreng, Chhun David, Taing Pa, Khat Tichhung</div>
            <div class="lines"></div>
            <h5 style="padding-bottom: 20px;" >Taught by Mr. Chim Bunthoeurn</h5> 
        </center>
    </div>
    <!-- body -->

    <!-- java -->
    <script src="apple.js"></script>
    <!-- java -->
</body>

</html>