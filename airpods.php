<!DOCTYPE html>
<html lang="en">

<head>
    <title>airpods</title> 
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="airpods_style.css">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter&family=Roboto&display=swap" rel="stylesheet">

        <script>

            window.onload = function() {
                document.getElementById('storage').value = "--";
                document.getElementById('memory').value = "--";
                document.getElementById('serialNumber').value = "12345678910";
                document.getElementById('subTotal').value = "$249.00";
                document.getElementById('totalShip').value = "249";
            };

            function submitForm(memory, storage, serialNumber, subTotal) {
                // set default of storage
                if (storage !== '') {
                    document.getElementById('storage').value = storage;
                } 
                if(memory !== '') {
                    document.getElementById('memory').value = memory;
                }
                if (subTotal !== '') {
                    document.getElementById('subTotal').value = subTotal;
                } 
                if (totalShip !== '') {
                    document.getElementById('totalShip').value = totalShip;
                } 
                document.getElementById('dataForm').submit();
            }
        </script> 
</head>

<body id="noscroll">

    <!-- menu bar -->
    <div class="menu_bg">
        <nav>
            <!-- destop size -->
            <ul class="destop">
                <li>
                    <a href="mainpage.html" class="apple_logo_white"></a>
                </li>
                <li>
                    <a href="mac.php">Mac</a>
                </li>
                <li>
                    <a href="iphone.php">iPhone</a>
                </li>
                <li>
                    <a href="iwatch.php">iWatch</a>
                </li>
                <li>
                    <a href="ipad.php">iPad</a>
                </li>
                <li>
                    <a style="text-decoration: underline;" href="airpods.php">Airpods</a>
                </li>
                <li>
                    <a href="search.html" class="search_logo"></a>
                </li>
                <li>
                    <a href="ship.php" class="ship_logo"></a>
                </li>
            </ul>
            <!-- destop size -->
        </nav>
    </div>
    <!-- menu bar --> 

    <!-- body -->
    <div class="airpods_bg">
        <nav class="text">
            <h1 style="font-size: 30px;">AirPods</h1>
        </nav>
        <nav class="learnmore">
            <img id="click" style="width: 80px; height: 15px; float: right;" src="images/learn_more.png">
                <div id="showdialog" class="dialog_bg">
                    <div class="dialog_shape">
                        <span class="close">
                            <img style="width: 50px; height: 50px;" src="images/close.png">
                        </span>
                        <h1 style="margin-top: 60px; margin-left: 20px; margin-right: 40px; color: white;" class="dialog_title">H2. More immersive by every measure.</h1>
                        <div style="padding-bottom: 0px;">
                            <video id="dialog_video1" style="border-radius: 20px; margin-right: 20px;" class="chip">
                                <source src="videos/H2.mp4" type="video/mp4" />
                            </video>
                        </div>
                        <h1 style="margin-left: 20px; margin-right: 40px; margin-bottom: 50px; color: white;" class="dialog_title_detail1_2_3">The Apple-designed H2 chip is the force behind the advanced audio performance of AirPods Pro, working with the driver and amplifier to create crisp, high-definition sound.</h1>
                        <div style="padding-bottom: 50px;">
                            <video id="dialog_video2" style="border-radius: 20px; width: 95%; margin-left: 20px; margin-right: 20px;" loop>
                                <source src="videos/airpods_dialog.mp4" type="video/mp4" />
                            </video>
                        </div>
                        <h1 style="margin-left: 20px; margin-right: 40px; margin-bottom: 20px; color: white;" class="dialog_title_detail1_2_3">It uses computational algorithms to deliver noise cancellation, superior three-dimensional sound, and efficient battery life — all at once.</h1>
                    </div>
                
                </div>
        </nav>
    </div>
    <div class="row">
        <div class="columnvideo">
            <img src="images/airpods.png" class="airpods">
            <div style="padding-bottom: 50px;">
                <video id="main_video" class="video" autoplay loop>
                    <source src="videos/airpods.mp4" type="video/mp4" />
                </video>
            </div>
        </div>
        <div class="columnchoose">
            <h1 style="font-size: 30px; margin-bottom: 50px;">Buy AirPods Pro(2nd generation)</h1>
            <img style="width: 85%;" src="images/break_line.png" class="break_line">
            <h1 style="font-size: 15px; ">Custom</h1>
            <img style="width: 85%; margin-top: 10px;" src="images/airpodsdetail.png">
            <nav class="total">
                <h1 style="font-size: 30px;" >Total :&nbsp;</h1>
                <h1 style="font-size: 30px;" >$249.00 </h1>
            </nav>
            <nav class="ship">
            <form id="shipForm" action="ship.php" method="post">
                    <!-- Hidden input fields to store selected memory and storage options -->
                    <input type="hidden" name="storage" id="storage">
                    <input type="hidden" name="memory" id="memory">
                    <input type="hidden" name="serialNumber" id="serialNumber">
                    <input type="hidden" name="model" id="model" value="Airpods">
                    <input type="hidden" name="subTotal" id="subTotal">
                    <input type="hidden" name="totalShip" id="totalShip">
                    <input type="image" style="width: 10%; border-radius: 40px; margin-right: 20px; margin-bottom: 50px; float: right;" src="images/cart.png" alt="Submit">
                </form>
                <!-- <a href="ship.php">
                    <img src="images/cart.png">
                </a> -->
            </nav>
        </div>
    </div>
    <div class="end">
        <center>
            <h3 style="padding-top: 20px;" >Preparing by Group 1</h3>
            <div style="padding-top: 20px;" >Srun kheangsreng, Chhun David, Taing Pa, Khat Tichhung</div>
            <div class="line"></div>
            <h5 style="padding-bottom: 20px;" >Taught by Mr. Chim Bunthoeurn</h5> 
        </center>
    </div>
    <!-- body -->

    <!-- java -->
    <script src="apple.js"></script>
    <!-- java -->
</body>

</html>