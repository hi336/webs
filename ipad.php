<!DOCTYPE html>
<html lang="en">

<head>
    <title>iPad</title>
        <meta charset="UTF-8"> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="ipad_style.css">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter&family=Roboto&display=swap" rel="stylesheet">
        <script>

            window.onload = function() {
                document.getElementById('memory').value = "11-inch display";
                document.getElementById('storage').value = "512GB SSD storage";
                document.getElementById('serialNumber').value = "12345678910";
                document.getElementById('subTotal').value = "$1,099.00";
                document.getElementById('totalShip').value = "1099";
            };

            function submitForm(memory, storage, serialNumber, subTotal, totalShip) {
                // set default of storage
                if (storage !== '') {
                    document.getElementById('storage').value = storage;
                } 
                if(memory !== '') {
                    document.getElementById('memory').value = memory;
                }
                if (subTotal !== '') {
                    document.getElementById('subTotal').value = subTotal;
                } 
                if (totalShip !== '') {
                    document.getElementById('totalShip').value = totalShip;
                } 
                document.getElementById('dataForm').submit();
            }
        </script> 
    </head>

<body id="noscroll">

    <!-- menu bar -->
    <div class="menu_bg">
        <nav>
            <!-- destop size -->
            <ul class="destop">
                <li>
                    <a href="mainpage.html" class="apple_logo_white"></a>
                </li>
                <li>
                    <a href="mac.php">Mac</a>
                </li>
                <li>
                    <a href="iphone.php">iPhone</a>
                </li>
                <li>
                    <a href="iwatch.php">iWatch</a>
                </li>
                <li>
                    <a style="text-decoration: underline;" href="ipad.php">iPad</a>
                </li>
                <li>
                    <a href="airpods.php">Airpods</a>
                </li>
                <li>
                    <a href="search.html" class="search_logo"></a>
                </li>
                <li>
                    <a href="ship.php" class="ship_logo"></a>
                </li>
            </ul>
            <!-- destop size -->
        </nav>
    </div>
    <!-- menu bar -->

    <!-- body -->
    <div class="ipad_bg">
        <nav class="text">
            <h1 style="font-size: 30px;">iPad</h1> 
        </nav>
        <nav class="learnmore">
            <img id="click" style="width: 80px; height: 15px; float: right;" src="images/learn_more.png">
            <div id="showdialog" class="dialog_bg"> 
                <div class="dialog_shape">
                    <span class="close">
                        <img style="width: 50px; height: 50px;" src="images/close.png">
                    </span>
                    <h1 style="margin-top: 60px; margin-left: 20px; margin-right: 40px; margin-bottom: 50px;" class="dialog_title">M2 chip.</h1>
                    <h1 style="margin-left: 20px; margin-right: 40px; margin-bottom: 20px;" class="dialog_title_detail1_2_3_4_5_6">Astonishing performance.</h1>
                    <img style="border-radius: 20px; width: 95%; margin-left: 20px; margin-right: 20px; margin-bottom: 50px;" src="images/ipad_dialog.png">
                    <h1 style="margin-left: 20px; margin-right: 40px;" class="dialog_title_detail1_2_3_4_5_6">Incredibly advanced displays.</h1>
                    <h1 class="ipads">
                        <img src="images/ipad_right_text.png" class="ipad_img">
                        M2 Chip 8-core CPU 10-core GPU 16-core Neural Engine A complete movie studio in your hands. The high-performance media engine on M2 accelerates ProRes encode and decode. So you can convert video projects to ProRes up to 3x faster than before.
                    </h1>
                    <h1 style="margin-left: 20px; margin-right: 40px; margin-bottom: 20px;" class="dialog_title_detail1_2_3_4_5_6">Super fast wireless connectivity.</h1>
                    <img style="margin-right: 50px; margin-bottom: 50px;" src="images/ipad_fast_charge.png" class="ipad1">
                    <h1 style="margin-left: 20px; margin-right: 40px; margin-bottom: 20px;" class="dialog_title_detail1_2_3_4_5_6">Next-level Apple Pencil capabilities.</h1>
                    <img style="border-radius: 20px; width: 95%; margin-left: 20px; margin-right: 20px; margin-bottom: 50px;" src="images/ipad_pencil.png">
                    <h1 style="margin-left: 20px; margin-right: 40px; margin-bottom: 20px;" class="dialog_title_detail1_2_3_4_5_6">Powerful new features in iPadOS.</h1>
                    <div style="padding-bottom: 50px;">
                        <video id="dialog_video1" style="border-radius: 20px; width: 95%; margin-left: 20px; margin-right: 20px;" loop>
                            <source src="videos/ipad_os.mp4" type="video/mp4" />
                        </video>
                    </div>
                    <h1 style="margin-left: 20px; margin-right: 40px; margin-bottom: 20px;" class="dialog_title_detail1_2_3_4_5_6">The ultimate iPad experience.</h1>
                </div>
            
            </div> 
        </nav>
    </div>
    <div class="row">
        <div class="columnvideo">
            <img src="images/ipad.png" class="ipad">
            <div style="padding-bottom: 50px;">
                <video id="main_video" class="video" autoplay loop>
                    <source src="videos/ipad.mp4" type="video/mp4" />
                </video>
            </div>
        </div>
        <div class="columnchoose">
            <h1 style="font-size: 30px; margin-bottom: 50px;">Buy iPad <Prog></Prog></h1>
            <img style="width: 85%;" src="images/break_line.png" class="break_line">
            <h1 style="font-size: 15px; ">Model</h1>
            <div onclick="Inch11(300), submitForm('11-inch display', '', '',totalPriceiPadSubTotal, totalPriceiPad)" id="Inch11" class="Inch11_bg">
                <div class="Inch11_bg_text">
                    <h1 style="font-size: 15px;">11-inch display</h1>
                </div>
                <div class="Inch11_bg_price">
                    <h1 id="Inch11_price" style="font-size: 15px;"></h1>
                </div>
            </div>
            <div onclick="Inch12(300); submitForm('12.9-inch display', '', '',totalPriceiPadSubTotal, totalPriceiPad)" id="Inch12" class="Inch12_bg">
                <div class="Inch12_bg_text">
                    <h1 style="font-size: 15px;">12.9-inch display</h1>
                </div>
                <div class="Inch12_bg_price">
                    <h1 id="Inch12_price" style="font-size: 15px;">+ $300.00</h1>
                </div>
            </div>
            <img style="width: 85%;" src="images/break_line.png" class="break_line">
            <h1 style="font-size: 15px; ">Storage</h1>
            <div onclick="GB512SSD(400); submitForm('', '512GB SSD storage', '', totalPriceiPadSubTotal, totalPriceiPad)" id="GB512SSD" class="GB512SSD_bg">
                <div class="GB512SSD_bg_text">
                    <h1 style="font-size: 15px;">512GB SSD storage</h1>
                </div>
                <div class="GB512SSD_bg_price">
                    <h1 id="GB512SSD_price" style="font-size: 15px;"></h1>
                </div>
            </div>
            <div onclick="TB1SSD(400); submitForm('', '1TB SSD storage', '', totalPriceiPadSubTotal, totalPriceiPad)" id="TB1SSD" class="TB1SSD_bg">
                <div class="TB1SSD_bg_text">
                    <h1 style="font-size: 15px;">1TB SSD storage</h1>
                </div>
                <div class="TB1SSD_bg_price">
                    <h1 id="TB1SSD_price" style="font-size: 15px;">+ $400.00</h1>
                </div>
            </div>
            <img style="width: 85%; margin-bottom: 0px;" src="images/break_line.png" class="break_line">
            <nav class="total">
                <h1 style="font-size: 30px;" >Total :&nbsp;</h1>
                <h1 id="total" style="font-size: 30px;" >$1,099.00</h1>
            </nav>
            <nav class="ship">
                <form id="shipForm" action="ship.php" method="post">
                    <!-- Hidden input fields to store selected memory and storage options -->
                    <input type="hidden" name="storage" id="storage">
                    <input type="hidden" name="memory" id="memory">
                    <input type="hidden" name="serialNumber" id="serialNumber">
                    <input type="hidden" name="model" id="model" value="iPad">
                    <input type="hidden" name="subTotal" id="subTotal">
                    <input type="hidden" name="totalShip" id="totalShip">
                    <input type="image" style="width: 10%; border-radius: 40px; margin-right: 20px; margin-bottom: 50px; float: right;" src="images/cart.png" alt="Submit">
                </form>
            </nav>
        </div>
    </div>
    <div class="end">
        <center>
            <h3 style="padding-top: 20px;" >Preparing by Group 1</h3>
            <div style="padding-top: 20px;" >Srun kheangsreng, Chhun David, Taing Pa, Khat Tichhung</div>
            <div class="line"></div>
            <h5 style="padding-bottom: 20px;" >Taught by Mr. Chim Bunthoeurn</h5> 
        </center>
    </div>
    <!-- body -->

    <!-- java -->
    <script src="apple.js"></script>
    <!-- java -->
</body>

</html>