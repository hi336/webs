/* ---------------------------------------- ship ---------------------------------------- */
function care(price) {
    document.getElementById("care").style.borderColor = "#00D958";
    document.getElementById("care1").style.background = "#00D958";
    document.getElementById("nocare").style.borderColor = "#C3C3C3";
    document.getElementById("nocare1").style.background = "#C3C3C3";
    document.getElementById("total").textContent = "\$" + price.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    document.getElementById("currentTotal").textContent = "\$" + price.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}
function nocare(price) {
    document.getElementById("care").style.borderColor = "#C3C3C3";
    document.getElementById("care1").style.background = "#C3C3C3";
    document.getElementById("nocare").style.borderColor = "#00D958";
    document.getElementById("nocare1").style.background = "#00D958";
    document.getElementById("total").textContent = "\$" + price.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    document.getElementById("currentTotal").textContent = "\$" + price.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}
function colorNatural_Titanium() {
    document.getElementById("colorname").textContent = "Natural_Titanium";
    document.getElementById("naturaltitanium").style.background = "#BBB5A9";
    document.getElementById("bluetitanium").style.background = "#ffffff";
    document.getElementById("whitetitanium").style.background = "#ffffff";
    document.getElementById("blacktitanium").style.background = "#ffffff";
}
function colorBlue_Titanium() {
    document.getElementById("colorname").textContent = "Blue_Titanium";
    document.getElementById("naturaltitanium").style.background = "#ffffff";
    document.getElementById("bluetitanium").style.background = "#49525F";
    document.getElementById("whitetitanium").style.background = "#ffffff";
    document.getElementById("blacktitanium").style.background = "#ffffff";
}
function colorWhite_Titanium() {
    document.getElementById("colorname").textContent = "White_Titanium";
    document.getElementById("naturaltitanium").style.background = "#ffffff";
    document.getElementById("bluetitanium").style.background = "#ffffff";
    document.getElementById("whitetitanium").style.background = "#edebe0";
    document.getElementById("blacktitanium").style.background = "#ffffff";
}
function colorBlack_Titanium() {
    document.getElementById("colorname").textContent = "Black_Titanium";
    document.getElementById("naturaltitanium").style.background = "#ffffff";
    document.getElementById("bluetitanium").style.background = "#ffffff";
    document.getElementById("whitetitanium").style.background = "#ffffff";
    document.getElementById("blacktitanium").style.background = "#47484A";
}
//Enable Submit Button
const submitButton = document.getElementById("submit");
const giftCard = document.getElementById("giftCard");
const promoCode = document.getElementById("promoCode");

//If promoCode input != null disable
giftCard.addEventListener("keyup", (e) => {
    promoCode.addEventListener("keyup", (e) => {
        const value = e.currentTarget.value;
        submitButton.disabled = false;
        document.getElementById("submit").style.background = "#4558FF";

        if (value == ""){
            submitButton.disabled = true;
        }
    })
})
//If Submit Button clicked
document.getElementById('submit').addEventListener('click', function(event) { 
    event.preventDefault();   

    //Save value
    const  email= document.getElementById("email").value;
    const fname = document.getElementById("fname").value;
    const lname = document.getElementById("lname").value;
    const card = document.getElementById("card").value;
    const country = document.getElementById("country").value;
    const roadAddress = document.getElementById("roadAddress").value;
    const townCity = document.getElementById("townCity").value;
    const postCode = document.getElementById("postCode").value;
    const giftCard = document.getElementById("giftCard").value;
    const promoCode = document.getElementById("promoCode").value;

    //Show save value to dialog
    document.getElementById('emailResult').textContent = email;
    document.getElementById('flnameResult').textContent = fname + " " + lname;
    document.getElementById('cardResult').textContent = card;
    document.getElementById('countryroadAddresstownCitypostCodeResult').textContent = country + " | " + roadAddress + " | " + townCity + " | " + postCode;
    document.getElementById('giftCardResult').textContent = giftCard;
    document.getElementById('promoCodeResult').textContent = promoCode;

    //Show save value in inspect console chrome
    console.log(email);  
    console.log(fname); 
    console.log(lname); 
    console.log(card); 
    console.log(country); 
    console.log(roadAddress); 
    console.log(townCity); 
    console.log(postCode); 
    console.log(giftCard); 
    console.log(promoCode); 

    //If promoCode != null clear all value in input field
    if (promoCode != null){
        document.getElementById("submit").style.background = "#FAFAFA";
        document.getElementById("receipt").style.background = "#4558FF";
        
        document.getElementById("email").value = "";
        document.getElementById("fname").value = "";
        document.getElementById("lname").value = "";
        document.getElementById("card").value = "";
        document.getElementById("country").value = "";
        document.getElementById("roadAddress").value = "";
        document.getElementById("townCity").value = "";
        document.getElementById("postCode").value = "";
        document.getElementById("model").value = "";
        document.getElementById("serialNumber").value = "";
        document.getElementById("memory").value = "";
        document.getElementById("storage").value = "";
        document.getElementById("giftCard").value = "";
        document.getElementById("promoCode").value = "";

        document.getElementById("care").style.borderColor = "#C3C3C3";
        document.getElementById("care1").style.background = "#C3C3C3";
        document.getElementById("nocare").style.borderColor = "#C3C3C3";
        document.getElementById("nocare1").style.background = "#C3C3C3";

        document.getElementById("naturaltitanium").style.background = "#BBB5A9";
        document.getElementById("bluetitanium").style.background = "#ffffff";
        document.getElementById("whitetitanium").style.background = "#ffffff";
        document.getElementById("blacktitanium").style.background = "#ffffff";

        document.getElementById("subTotal").value = "";
        document.getElementById("total").value = "";
    }

})
/* ---------------------------------------- dialog ---------------------------------------- */
    var noscroll = document.getElementById("noscroll"); 

    var main_video = document.getElementById("main_video");
    var dialog_video1 = document.getElementById("dialog_video1");
    var dialog_video2 = document.getElementById("dialog_video2");
    var dialog_video3 = document.getElementById("dialog_video3");

    var modal = document.getElementById("showdialog");
            
    var btn = document.getElementById("click");

    var span = document.getElementsByClassName("close")[0];

    btn.onclick = function() {
        modal.style.display = "block";
        noscroll.style.position = "fixed";
        main_video.currentTime = 0;
        main_video.pause();
        dialog_video1.play();
        dialog_video2.play();
        dialog_video3.play();
        dialog_video4.play();
    }

    span.onclick = function() {
        modal.scrollTo(0,0);
        modal.style.display = "none";
        noscroll.style.position = "";
        main_video.play();
        dialog_video1.currentTime = 0;
        dialog_video1.pause();
        dialog_video2.currentTime = 0;
        dialog_video2.pause();
        dialog_video3.currentTime = 0;
        dialog_video3.pause();
        dialog_video4.currentTime = 0;
        dialog_video4.pause();
    }
/* -------------------------------------------------------------------------------------- */