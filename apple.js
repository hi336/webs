/* ---------------------------------------- mac ---------------------------------------- */
var totalPriceMac = 1499;
var isClickGB8 = true;
var isClickGB16 = false;
var isClickGB512 = true;
var isClickTB1 = false;

function GB8(price){
    if(!isClickGB8){
        isClickGB8 = true;
        isClickGB16 = false;
        document.getElementById("GB8").style.borderColor = "#4558FF";
        document.getElementById("GB16").style.borderColor = "#C3C3C3";
        document.getElementById("GB8_price").textContent = "";
        document.getElementById("GB16_price").textContent = "+ $200.00";
        totalPriceMac = totalPriceMac - price 
        document.getElementById("total").innerHTML = "\$" + totalPriceMac.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        totalPriceMacSubTotal = "\$" + totalPriceMac.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }
}

function GB16(price){
    if(!isClickGB16){
        isClickGB8 = false;
        isClickGB16 = true;
        document.getElementById("GB8").style.borderColor = "#C3C3C3";
        document.getElementById("GB16").style.borderColor = "#4558FF";
        document.getElementById("GB8_price").textContent = "- $200.00";
        document.getElementById("GB16_price").textContent = "";
        totalPriceMac = totalPriceMac + price
        document.getElementById("total").innerHTML = "\$" + totalPriceMac.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        totalPriceMacSubTotal = "\$" + totalPriceMac.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }
}

function GB512(price){
    if(!isClickGB512){
        isClickGB512 = true;
        isClickTB1 = false;
        document.getElementById("GB512").style.borderColor = "#4558FF";
        document.getElementById("TB1").style.borderColor = "#C3C3C3";
        document.getElementById("GB512_price").textContent = "";
        document.getElementById("TB1_price").textContent = "+ $200.00";
        totalPriceMac = totalPriceMac - price
        document.getElementById("total").innerHTML = "\$" + totalPriceMac.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        totalPriceMacSubTotal = "\$" + totalPriceMac.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }
}

function TB1(price){
    if(!isClickTB1){
        isClickGB512 = false;
        isClickTB1 = true;
        document.getElementById("GB512").style.borderColor = "#C3C3C3";
        document.getElementById("TB1").style.borderColor = "#4558FF";
        document.getElementById("GB512_price").textContent = "- $200.00";
        document.getElementById("TB1_price").textContent = "";
        totalPriceMac = totalPriceMac + price
        document.getElementById("total").innerHTML = "\$" + totalPriceMac.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        totalPriceMacSubTotal = "\$" + totalPriceMac.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }
}

/* ---------------------------------------- iphone ---------------------------------------- */

var totalPriceiPhone = 999;
var isClickGB128 = true;
var isClickGB256 = false;


function GB128(price) {
    if(!isClickGB128){
        isClickGB128 = true;
        isClickGB256 = false;
        document.getElementById("GB128").style.borderColor = "#4558FF";
        document.getElementById("GB256").style.borderColor = "#C3C3C3";
        document.getElementById("GB128_price").textContent = "";
        document.getElementById("GB256_price").textContent = "+ $100.00";
        totalPriceiPhone = totalPriceiPhone - price
        document.getElementById("total").innerHTML = "\$" + totalPriceiPhone.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        totalPriceiPhoneSubTotal = "\$" + totalPriceiPhone.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }
}
function GB256(price) {
    if(!isClickGB256){
        isClickGB128 = false;
        isClickGB256 = true;
        document.getElementById("GB128").style.borderColor = "#C3C3C3";
        document.getElementById("GB256").style.borderColor = "#4558FF";
        document.getElementById("GB128_price").textContent = "- $100.00";
        document.getElementById("GB256_price").textContent = "";
        totalPriceiPhone = totalPriceiPhone + price
        document.getElementById("total").innerHTML = "\$" + totalPriceiPhone.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        totalPriceiPhoneSubTotal = "\$" + totalPriceiPhone.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }
}
/* ---------------------------------------- iwatch ---------------------------------------- */
function orange() {
    document.getElementById("orange").style.borderColor = "#4558FF";
    document.getElementById("white").style.borderColor = "transparent";
    document.getElementById("green").style.borderColor = "transparent";
    document.getElementById("image").src = "images/iwatch_orange.png"
}
function white() {
    document.getElementById("orange").style.borderColor = "transparent";
    document.getElementById("white").style.borderColor = "#4558FF";
    document.getElementById("green").style.borderColor = "transparent";
    document.getElementById("image").src = "images/iwatch_white.png"
}
function green() {
    document.getElementById("orange").style.borderColor = "transparent";
    document.getElementById("white").style.borderColor = "transparent";
    document.getElementById("green").style.borderColor = "#4558FF";
    document.getElementById("image").src = "images/iwatch_green.png"
}
/* ---------------------------------------- ipad ---------------------------------------- */
var totalPriceiPad = 1099
var isClickInch11 = true;
var isClickInch12 = false;
var isClickGB512SSD = true;
var isClickTB1SSD = false;

function Inch11(price) {
    if(!isClickInch11){
        isClickInch11 = true;
        isClickInch12 = false;
        document.getElementById("Inch11").style.borderColor = "#4558FF";
        document.getElementById("Inch12").style.borderColor = "#C3C3C3";
        document.getElementById("Inch11_price").textContent = "";
        document.getElementById("Inch12_price").textContent = "+ $300.00";
        totalPriceiPad = totalPriceiPad - price
        document.getElementById("total").innerHTML = "\$" + totalPriceiPad.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        totalPriceiPadSubTotal = "\$" + totalPriceiPad.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }
}
function Inch12(price) {
    if(!isClickInch12){
        isClickInch11 = false;
        isClickInch12 = true;
        document.getElementById("Inch11").style.borderColor = "#C3C3C3";
        document.getElementById("Inch12").style.borderColor = "#4558FF";
        document.getElementById("Inch11_price").textContent = "- $300.00";
        document.getElementById("Inch12_price").textContent = "";
        totalPriceiPad = totalPriceiPad + price
        document.getElementById("total").innerHTML = "\$" + totalPriceiPad.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        totalPriceiPadSubTotal = "\$" + totalPriceiPad.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }
}
function GB512SSD(price) {
    if(!isClickGB512SSD){
        isClickGB512SSD = true;
        isClickTB1SSD = false;
        document.getElementById("GB512SSD").style.borderColor = "#4558FF";
        document.getElementById("TB1SSD").style.borderColor = "#C3C3C3";
        document.getElementById("GB512SSD_price").textContent = "";
        document.getElementById("TB1SSD_price").textContent = "+ $400.00";
        totalPriceiPad = totalPriceiPad - price
        document.getElementById("total").innerHTML = "\$" + totalPriceiPad.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        totalPriceiPadSubTotal = "\$" + totalPriceiPad.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }
}
function TB1SSD(price) {
    if(!isClickTB1SSD){
        isClickGB512SSD = false;
        isClickTB1SSD = true;
        document.getElementById("GB512SSD").style.borderColor = "#C3C3C3";
        document.getElementById("TB1SSD").style.borderColor = "#4558FF";
        document.getElementById("GB512SSD_price").textContent = "- $400.00";
        document.getElementById("TB1SSD_price").textContent = "";
        totalPriceiPad = totalPriceiPad + price
        document.getElementById("total").innerHTML = "\$" + totalPriceiPad.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        totalPriceiPadSubTotal = "\$" + totalPriceiPad.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    }
}
/* ---------------------------------------- dialog ---------------------------------------- */
var noscroll = document.getElementById("noscroll"); 

var main_video = document.getElementById("main_video");
var dialog_video1 = document.getElementById("dialog_video1");
var dialog_video2 = document.getElementById("dialog_video2");
var dialog_video3 = document.getElementById("dialog_video3");

var modal = document.getElementById("showdialog");
        
var btn = document.getElementById("click");

var span = document.getElementsByClassName("close")[0];

btn.onclick = function() {
    modal.style.display = "block";
    noscroll.style.position = "fixed";
    main_video.currentTime = 0;
    main_video.pause();
    dialog_video1.play();
    dialog_video2.play();
    dialog_video3.play();
    dialog_video4.play();
}

span.onclick = function() {
    modal.scrollTo(0,0);
    modal.style.display = "none";
    noscroll.style.position = "";
    main_video.play();
    dialog_video1.currentTime = 0;
    dialog_video1.pause();
    dialog_video2.currentTime = 0;
    dialog_video2.pause();
    dialog_video3.currentTime = 0;
    dialog_video3.pause();
    dialog_video4.currentTime = 0;
    dialog_video4.pause();
}
/* -------------------------------------------------------------------------------------- */