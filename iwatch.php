<!DOCTYPE html>
<html lang="en">

<head>
    <title>iWatch</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="iwatch_style.css">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter&family=Roboto&display=swap" rel="stylesheet">
         
        <script>

            window.onload = function() {
                document.getElementById('storage').value = "--";
                document.getElementById('memory').value = "--";
                document.getElementById('serialNumber').value = "12345678910";
                document.getElementById('subTotal').value = "$799.00";
                document.getElementById('totalShip').value = "799";
            };

            function submitForm(memory, storage, serialNumber, subTotal) {
                // set default of storage
                if (storage !== '') {
                    document.getElementById('storage').value = storage;
                } 
                if(memory !== '') {
                    document.getElementById('memory').value = memory;
                }
                if (subTotal !== '') {
                    document.getElementById('subTotal').value = subTotal;
                } 
                if (totalShip !== '') {
                    document.getElementById('totalShip').value = totalShip;
                } 
                document.getElementById('dataForm').submit();
            }
        </script> 
</head>

<body id="noscroll">

    <!-- menu bar -->
    <div class="menu_bg">
        <nav>
            <!-- destop size -->
            <ul class="destop">
                <li>
                    <a href="mainpage.html" class="apple_logo_white"></a>
                </li>
                <li>
                    <a href="mac.php">Mac</a>
                </li>
                <li>
                    <a href="iphone.php">iPhone</a>
                </li>
                <li>
                    <a style="text-decoration: underline;" href="iwatch.php">iWatch</a>
                </li>
                <li>
                    <a href="ipad.php">iPad</a>
                </li>
                <li>
                    <a href="airpods.php">Airpods</a>
                </li>
                <li>
                    <a href="search.html" class="search_logo"></a>
                </li>
                <li>
                    <a href="ship.php" class="ship_logo"></a>
                </li>
            </ul>
            <!-- destop size -->
        </nav>
    </div>
    <!-- menu bar -->

    <!-- body -->
    <div class="iwatch_bg">
        <nav class="text">
            <h1 style="font-size: 30px;">iWatch</h1>
        </nav>
        <nav class="learnmore">
            <img id="click" style="width: 80px; height: 15px; float: right;" src="images/learn_more.png">
            <div id="showdialog" class="dialog_bg">
                <div class="dialog_shape">
                    <span class="close">
                        <img style="width: 50px; height: 50px;" src="images/close.png">
                    </span>
                    <h1 style="margin-top: 60px; margin-left: 20px; margin-right: 40px; margin-bottom: 50px;" class="dialog_title">Next level adventure.</h1>
                    <img style="border-radius: 20px; width: 95%; margin-left: 20px; margin-right: 20px; margin-bottom: 50px;" src="images/iwatch_dialog.png">
                    <h1 style="margin-left: 20px; margin-right: 40px; margin-bottom: 50px;" class="dialog_title_detail1">The most rugged and capable Apple Watch pushes the limits again. Featuring the all-new S9 SiP. A magical new way to use your watch without touching the screen. The brightest Apple display ever. And now you can choose a case and band combination that is carbon neutral.</h1>
                    <img style="background: transparent; border-radius: 20px; width: 95%; margin-left: 20px; margin-right: 20px; margin-bottom: 20px;" src="images/iwatch_with_iphone.png">
                </div>
            
            </div>
        </nav>
    </div>
    <div class="row">
        <div class="columnvideo">
            <img id="image" src="images/iwatch_orange.png" class="iwatch">
            <div style="padding-bottom: 50px;">
                <video id="main_video" class="video" autoplay loop>
                    <source src="videos/iwatch.mp4" type="video/mp4" />
                </video>
            </div>
        </div> 
        <div class="columnchoose">
            <h1 style="font-size: 30px; margin-bottom: 50px;">Buy Apple Watch Ultra</h1>
            <img style="width: 85%;" src="images/break_line.png" class="break_line">
            <h1 style="font-size: 15px; ">Band</h1>
            <nav onclick="orange()" id="orange" class="orange_bg" >
                <img style="border-radius: 5px; width: 100px; height: 100%; padding: 2px;" src="images/orange.png">
            </nav>
            <nav onclick="white()" id="white" class="white_bg" >
                <img style="border-radius: 5px; width: 100px; height: 100%; padding: 2px;" src="images/white.png">
            </nav>
            <nav onclick="green()" id="green" class="green_bg" >
                <img style="border-radius: 5px; width: 100px; height: 100%; padding: 2px;" src="images/green.png">
            </nav>
            <nav class="total">
                <h1 style="font-size: 30px;" >Total :&nbsp;</h1>
                <h1 style="font-size: 30px;" >$799.00 </h1>
            </nav>
            <nav class="ship">
            <form id="shipForm" action="ship.php" method="post">
                    <!-- Hidden input fields to store selected memory and storage options -->
                    <input type="hidden" name="storage" id="storage">
                    <input type="hidden" name="memory" id="memory">
                    <input type="hidden" name="serialNumber" id="serialNumber">
                    <input type="hidden" name="model" id="model" value="iWatch">
                    <input type="hidden" name="subTotal" id="subTotal">
                    <input type="hidden" name="totalShip" id="totalShip">
                    <input type="image" style="width: 10%; border-radius: 40px; margin-right: 20px; margin-bottom: 50px; float: right;" src="images/cart.png" alt="Submit">
                </form>
            </nav>
        </div>
    </div> 
    <div class="end">
        <center>
            <h3 style="padding-top: 20px;" >Preparing by Group 1</h3>
            <div style="padding-top: 20px;" >Srun kheangsreng, Chhun David, Taing Pa, Khat Tichhung</div>
            <div class="line"></div>
            <h5 style="padding-bottom: 20px;" >Taught by Mr. Chim Bunthoeurn</h5> 
        </center>
    </div>
    <!-- body -->

    <!-- java -->
    <script src="apple.js"></script>
    <!-- java -->
</body>

</html>