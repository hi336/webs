<!DOCTYPE html>
<html lang="en">

    <head>
        <title>ship</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="ship.css">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter&family=Roboto&display=swap" rel="stylesheet">

        <!-- Testing  -->
        <?php   
            // session_start();
            if ($_SERVER['REQUEST_METHOD'] === 'POST') {
                // Request was made using the POST method
                // Your PHP code for handling POST data goes here
                // For example:
                $storage = $_POST['storage'];
                $memory = $_POST['memory'];
                $model = $_POST['model'];
                $subTotal = $_POST['subTotal'];
                $total = $_POST['totalShip'];
                $serialNumber = $_POST['serialNumber'];
                // Process the received data

                // echo "MMEEEE" . $_POST;
            }
        ?>


    </head>

    <body id="noscroll">

        <!-- menu bar -->
        <div class="menu_bg">
            <nav>
                <!-- destop size -->
                <ul class="destop">
                    <li>
                        <a href="mainpage.html" class="apple_logo_white"></a>
                    </li>
                    <li>
                        <a href="mac.php">Mac</a>
                    </li>
                    <li>
                        <a href="iphone.php">iPhone</a>
                    </li>
                    <li>
                        <a href="iwatch.php">iWatch</a>
                    </li>
                    <li>
                        <a href="ipad.php">iPad</a>
                    </li>
                    <li>
                        <a href="airpods.php">Airpods</a>
                    </li>
                    <li>
                        <a href="search.php" class="search_logo"></a>
                    </li>
                    <li>
                        <a href="ship.php" class="ship_logo"></a>
                    </li>
                </ul>
                <!-- destop size -->
            </nav>
        </div>
        <!-- menu bar -->

        <!-- body -->
        <form id="form" action="ship.php" method="post">                
            <div style="margin-top: 60px; margin-left: auto; margin-right: auto;" class="row">
                <!-- Info. -->
                <div class="column1">
                    <h1 style="font-size: 30px; padding-top: 20px; padding-bottom: 30px;">Info.</h1>
                    <nav>
                        <label for="email" style="font-weight: bold;">Email Address</label>
                        <input type="text" id="email" placeholder="email address" name="email">
                    </nav>
                    <div style="padding-top: 20px; padding-bottom: 20px;" class="row">
                        <div style="width: 50%; padding-right: 2.5%;" class="columnname">
                            <label for="fname" style="font-weight: bold;">First Name</label>
                            <input type="text" id="fname" placeholder="first name" name="firstName" required>
                        </div>
                        <div style="width: 50%; padding-left: 2.5%;" class="columnname">
                            <label for="lname" style="font-weight: bold;">Last Name</label>
                            <input type="text" id="lname" placeholder="last name" name="lastName" required>
                        </div>  
                    </div>
                    <label for="card" style="font-weight: bold;" class="box">
                        Apple / Credit / Debit Card
                        <img  src="images/cards.png"/>
                    </label>
                    <input type="text" id="card" placeholder="card" name="card" required>
                    <nav style="padding-top: 20px;">      
                        <label for="country" style="font-weight: bold;">Country</label>
                        <input type="text" id="country" placeholder="country" name="country" required>  
                    </nav>
                    <nav style="padding-top: 20px;">   
                        <label for="roadaddress" style="font-weight: bold;">Road Address</label>
                        <input type="text" id="roadAddress" placeholder="road address" name="roadAddress" required> 
                    </nav>
                    <nav style="padding-top: 20px;">  
                        <label for="town / city" style="font-weight: bold;">Town / City</label>
                        <input type="text" id="townCity" placeholder="town / city" name="townCity" required> 
                    </nav>
                    <nav style="padding-top: 20px; padding-bottom: 20px;">  
                        <label for="postcode" style="font-weight: bold;">Postcode</label>
                        <input type="text" id="postCode" placeholder="postcode" name="postCode" required>   
                    </nav>   
                </div>
                <!-- Info. -->

                <!-- Apple Cash Balance -->
                <div class="column2">
                    <h1 style="font-size: 30px; padding-top: 20px; padding-bottom: 30px; display: flex;" class="apple">Apple Cash Balance</h1>
                    <nav>
                        <label for="model" style="font-weight: bold;">Model</label>
                        <input type="text" id="model" placeholder="model" name="model" value="<?php echo $model ?>">
                    </nav>
                    <nav style="padding-top: 20px;">  
                        <label for="serial number" style="font-weight: bold;">Serial Number</label>
                        <input type="text" id="serialNumber" placeholder="serial number" name="serialNumber" value="<?php echo $serialNumber ?>">
                    </nav>
                    <nav style="padding-top: 20px; margin-bottom: 27px;" class="ship">  
                        <label style="font-weight: bold;">Shipping Care</label>
                        <div class="row">

                            <div class="columncarepickrow1">
                                <div>
                                    <div id="care" class="columncarepick">
                                        <div id="care1" class="columncarepick1_2"></div>
                                    </div>
                                    <h4 style="margin-top: 40px; color: #717171;">We are provide you with a good care of your products privately.</h4>
                                    <h4 style="margin-top: 40px;">+ 10$</h4>
                                </div>
                            </div>
                            
                            <div class="columncarepickrowline">
                                <div class="columncarepickrowline1"></div>
                            </div>

                            <div class="columncarepickrow2">
                                <div>
                                    <div id="nocare" class="columncarepick">
                                        <div id="nocare1" class="columncarepick1_2"></div>
                                    </div>
                                    <h4 style="margin-top: 40px; color: #717171;" >We are provide you with a good care of your products with others.</h4>
                                    <h4 style="margin-top: 40px;" >+ 0$</h4>
                                </div>
                            </div>
                            
                        </div>
                    </nav>
                    <nav>
                        <label for="gift card" style="font-weight: bold;">Gift Card</label>
                        <input type="text" id="giftCard" placeholder="gift card" name="giftcard" required>
                    </nav>
                    <nav style="padding-top: 20px;">  
                        <label for="promo code" style="font-weight: bold;">Promo Code</label>
                        <input type="text" id="promoCode" placeholder="promo code" name="promoCode" required> 
                    </nav>
                </div>
                <div class="column3">
                    <nav>  
                        <label for="memory" style="font-weight: bold;">Memory</label>
                        <input type="text" id="memory" placeholder="memory" name="memory" value="<?php echo $memory ?>" /> 
                    </nav>
                    <nav style="padding-top: 20px;">  
                        <label for="storage" style="font-weight: bold;">Storage</label>
                        <input type="text" id="storage" placeholder="storage" name="storage" value="<?php echo $storage ?>" /> 
                    </nav>
                    <nav style="padding-top: 20px; margin-bottom: 20px">  
                        <label style="font-weight: bold;">Color : </label>
                        <label id="colorname" style="font-weight: bold;">Natural_Titanium</label>
                        <div class="row" style="margin-top: 32px">
                            <div class="columncolorpickbg">
                                <div onclick="colorNatural_Titanium()" class="columncolorpicknatural">
                                    <div id="naturaltitanium" class="columncolorpicknaturaltitanium">
                                    </div>
                                </div>
                            </div>
                            <div class="columncolorpickbg">
                                <div onclick="colorBlue_Titanium()" class="columncolorpickblue">
                                    <div id="bluetitanium" class="columncolorpickbluetitanium"></div>
                                </div>
                            </div>
                            <div class="columncolorpickbg">
                                <div onclick="colorWhite_Titanium()" class="columncolorpickwhite">
                                    <div id="whitetitanium" class="columncolorpickwhitetitanium"></div>
                                </div>
                            </div>
                            <div class="columncolorpickbg">
                                <div onclick="colorBlack_Titanium()" class="columncolorpickblack">
                                    <div id="blacktitanium" class="columncolorpickblacktitanium"></div>
                                </div>
                            </div>
                        </div>
                    </nav>
                    <nav style="padding-top: 20px; display: flex; justify-content: center;">  
                        <img style="height: 60px; width: fit-content;" src="images/carbon.png"> 
                    </nav>
                    <img style="width: 100%; margin-top: 42px;" src="images/break_line.png" class="break_line">
                    <nav class="amount_info">
                        <h4 id="subTotal" class="amount_info_sub_ship_total">
                            <?php echo $subTotal ?>
                            <h4>Subtotal</h4>
                        </h4>
                        <h4 style="margin-top: 20px;" class="amount_info_sub_ship_total">
                            FREE
                            <h4 style="margin-top: 20px;">Shipping</h4>
                        </h4>
                    </nav>
                    <img style="width: 100%; margin-bottom: 0px;" src="images/break_line.png" class="break_line">
                    <nav class="amount_info">
                        <h1 id="total" class="amount_info_sub_ship_total">
                            --
                            <h1>Total</h1>
                        </h1> 
                    </nav>
                    <nav class="amount_info">
                        <h4 class="checkout" >
                            Have a Great Day.
                            <button id="submit" disabled="disabled" class="button" form="form" type="submit">Submist</button>
                            <nav id="receipt" class="clickcheckout">
                                <h5 id="click" style="width: 80px; height: 15px; font-size: 16px; text-align: center; margin-bottom: 5px; color: white;">Receipt</h5>
                                <div id="showdialog" class="dialog_bg">
                                    <div class="dialog_shape">
                                        <span class="close">
                                            <img style="width: 50px; height: 50px;" src="images/close.png" class="img">
                                        </span>
                                        <img style="width: 150px; height: 150px; margin-top: 100px; margin-bottom: 50px;" src="images/apple-logo-black.svg">
                                        <h1 style="margin-right: 40px; margin-bottom: 20px;" class="dialog_title_detail1">Apple Store, Phnom Penh</h1>
                                        <h1 id="countryroadAddresstownCitypostCodeResult" style="margin-right: 40px; margin-bottom: 20px;" class="dialog_title_detail1"></h1>
                                        <h1 id="cardResult" style="margin-right: 40px; margin-bottom: 20px;" class="dialog_title_detail1"></h1>
                                        <div class="line_black"></div>
                                        <h1 style="margin-right: 40px; font-weight: 500px; margin-bottom: 30px;" class="dialog_title_detail1">September 26, 2019 07:18</h1>
                                        <h1 id="emailResult" style="margin-right: 40px; margin-bottom: 20px;" class="dialog_title_detail1"></h1>
                                        <h1 id="flnameResult" style="margin-right: 40px; margin-bottom: 20px;" class="dialog_title_detail1"></h1>
                                        <div class="line_black" style="height: 2.5px;"></div>
                                        <p class="left1">
                                            <?php $model = filter_input(INPUT_POST, 'model'); echo $model; ?>
                                            <span class="right">
                                                <?php echo $subTotal ?>
                                            </span>
                                        </p>
                                        <div class="line_black" style="height: 2.5px;"></div>
                                        <p class="left2">
                                            Shipping
                                            <span class="right">
                                                FREE
                                            </span>
                                        </p>
                                        <br/>
                                        <p class="left2">
                                            Serial Number
                                            <span class="right">
                                                <?php echo $serialNumber ?>
                                            </span>
                                        </p>
                                        <br/>
                                        <p class="left2">
                                            Shipping Care
                                            <span class="right">
                                                Checked
                                            </span>
                                        </p>
                                        <br/>
                                        <p class="left2">
                                            Gift Card
                                            <span id="giftCardResult" class="right"></span>
                                        </p>
                                        <br/>
                                        <p class="left2">
                                            Promode Code
                                            <span id="promoCodeResult" class="right"></span>
                                        </p>
                                        <br/>
                                        <p class="left2">
                                            Subtotal
                                            <span class="right">
                                                <?php echo $subTotal ?>
                                            </span>
                                        </p>
                                        <br/>
                                        <p class="left2">
                                            Total
                                            <span id="currentTotal" class="right">
                                            </span>
                                        </p>
                                        <h1 style="margin-right: 40px; margin-top: 20px;" class="dialog_title_detail1">Gift Card remaining balance may exclude any pending orders placed through the Apple Online Store or Apple Telesales</h1>
                                        <div class="line_black" style="height: 5px; margin-bottom: 100px;"></div>
                                        <h1 style="margin-right: 40px;" class="dialog_title_detail1">$https://www.apple.com/legal/sales-support/sales-policies/retail.htm%7c</h1>
                                        <br/>
                                        <h1 style="margin-right: 40px;" class="dialog_title_detail1">Tell us about your experience at the Apple Store</h1>
                                        <br/>
                                        <h1 style="margin-right: 40px;" class="dialog_title_detail1">Visit www.apple.com/feedback/retail.html</h1>
                                    </div>
                                </div> 
                            </nav>
                        </h4>
                    </nav>
                </div>
                <!-- Apple Cash Balance -->
            </div>
        </form>
        <div class="end">
            Save on Mac or iPad with education pricing and get a gift card up to $150. Gift card offer ends October 2.
        </div>
        <div class="ends">
            <center>
                <h3 style="padding-top: 20px;" >Preparing by Group 1</h3>
                <div style="padding-top: 20px;" >Srun kheangsreng, Chhun David, Taing Pa, Khat Tichhung</div>
                <div class="line"></div>
                <h5 style="padding-bottom: 20px;" >Taught by Mr. Chim Bunthoeurn</h5> 
            </center>
        </div>
        <!-- body -->


        <!-- java -->
        <script src="apples.js"></script>
        <script>
            var totals = <?php echo $total ?>;
            var current_total = totals;

            document.getElementById("care").addEventListener('click', function(event) {
                if(current_total == totals){
                    current_total = current_total + 10
                } else {
                    current_total = current_total
                }
                care(current_total)
            })

            document.getElementById("nocare").addEventListener('click', function(event) {
                if(current_total != totals){
                    current_total = current_total - 10
                } else {
                    current_total = current_total
                }
                nocare(current_total)
            })

        </script>
        <!-- java -->
        
    </body>

</html>
