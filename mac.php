<!DOCTYPE html>
<html lang="en">

<head>  
    <title>Mac</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="mac_style.css">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Inter&family=Roboto&display=swap" rel="stylesheet">

        <script>

            window.onload = function() {
                document.getElementById('memory').value = "8GB unified memory";
                document.getElementById('storage').value = "512GB SSD storage";
                document.getElementById('serialNumber').value = "12345678910";
                document.getElementById('subTotal').value = "$1,499.00";
                document.getElementById('totalShip').value = "1499";
            };

            function submitForm(memory, storage, serialNumber, subTotal, totalShip) {
                // set default of storage
                if (storage !== '') {
                    document.getElementById('storage').value = storage;
                } 
                if(memory !== '') {
                    document.getElementById('memory').value = memory;
                }
                if (subTotal !== '') {
                    document.getElementById('subTotal').value = subTotal;
                } 
                if (totalShip !== '') {
                    document.getElementById('totalShip').value = totalShip;
                } 
                document.getElementById('dataForm').submit();
            }
        </script>  
              
</head>

<body id="noscroll">
 
    <!-- menu bar -->
    <div class="menu_bg">
        <nav>
            <!-- destop size -->
            <ul class="destop">
                <li>
                    <a href="mainpage.html" class="apple_logo_white"></a>
                </li>
                <li>
                    <a style="text-decoration: underline;" href="mac.php">Mac</a>
                </li>
                <li>
                    <a href="iphone.php">iPhone</a>
                </li>
                <li>
                    <a href="iwatch.php">iWatch</a>
                </li>
                <li>
                    <a href="ipad.php">iPad</a>
                </li>
                <li>
                    <a href="airpods.php">Airpods</a>
                </li>
                <li>
                    <a href="search.html" class="search_logo"></a>
                </li>
                <li>
                    <a href="ship.php" class="ship_logo"></a>
                </li>
            </ul>
            <!-- destop size -->
        </nav>
    </div>
    <!-- menu bar -->

    <!-- body -->
    <div class="mac_bg">
        <nav class="text">
            <h1 style="font-size: 30px;">MacBook Air</h1>
        </nav>
        <nav class="learnmore">
            <img id="click" style="width: 80px; height: 15px; float: right;" src="images/learn_more.png">
            <div id="showdialog" class="dialog_bg">
                <div class="dialog_shape">
                    <span class="close">
                        <img style="width: 50px; height: 50px;" src="images/close.png" class="img">
                    </span>
                    <h1 style="margin-top: 60px; margin-left: 20px; margin-right: 40px; margin-bottom: 50px;" class="dialog_title">MacBook Air</h1>
                    <h1 style="margin-left: 20px; margin-right: 40px; margin-bottom: 20px;" class="dialog_title_detail1_2_3">is all you — pick your size, pick your color, then go.</h1>
                    <div style="padding-bottom: 50px;">
                        <video id="dialog_video1" style="border-radius: 20px; width: 95%; margin-left: 20px; margin-right: 20px;" loop>
                            <source src="videos/mac_color.mp4" type="video/mp4" />
                        </video>
                    </div>
                    <h1 style="margin-left: 20px; margin-right: 40px; margin-bottom: 20px;" class="dialog_title_detail1_2_3">Whichever model you choose, it's built with the planet in mind, with a durable 100 percent recycled aluminum enclosure. </h1>
                    <div style="padding-bottom: 50px;">
                        <video id="dialog_video2" style="border-radius: 20px; width: 95%; margin-left: 20px; margin-right: 20px;" loop>
                            <source src="videos/aluminum.mp4" type="video/mp4" />
                        </video>
                    </div>
                    <h1 style="margin-left: 20px; margin-right: 40px; margin-bottom: 20px;" class="dialog_title_detail1_2_3">And a finless design means it stays silent even under intense workloads.</h1>
                    <img style="border-radius: 20px; width: 95%; margin-left: 20px; margin-right: 20px; margin-bottom: 20px;" src="images/mac_thin.png">
                </div>
            </div>  
        </nav>
    </div>
    <div class="row">
        <div class="columnvideo">
            <img src="images/mac.png" class="mac">
            <div style="padding-bottom: 50px;">
                <video id="main_video" class="video" autoplay loop>
                    <source src="videos/mac.mp4" type="video/mp4" class="video"/>
                </video>
            </div>
        </div>
        <div class="columnchoose">
            <h1 style="font-size: 30px; margin-bottom: 50px;">Buy MacBook Air</h1>
            <img style="width: 85%;" src="images/break_line.png" class="break_line">
            <h1 style="font-size: 15px; ">Memory</h1>
            <div onclick="GB8(200); submitForm('8GB unified memory', '', '', totalPriceMacSubTotal, totalPriceMac)" id="GB8" class="GB8_bg">
                <div class="GB8_bg_text">
                    <h1 style="font-size: 15px;">8GB unified memory</h1>
                </div>
                <div class="GB8_bg_price">
                    <h1 id="GB8_price" style="font-size: 15px;"></h1>
                </div>
            </div>
            <div onclick="GB16(200); submitForm('16GB unified memory', '', '',totalPriceMacSubTotal, totalPriceMac)" id="GB16" class="GB16_bg">
                <div class="GB16_bg_text">
                    <h1 style="font-size: 15px;">16GB unified memory</h1>
                </div>
                <div class="GB16_bg_price">
                    <h1 id="GB16_price" style="font-size: 15px;">+ $200.00</h1>
                </div>
            </div>
            <img style="width: 85%;" src="images/break_line.png" class="break_line">
            <h1 style="font-size: 15px; ">Storage</h1>
            
            <div onclick="GB512(200); submitForm('', '512GB SSD storage', '', totalPriceMacSubTotal, totalPriceMac)" id="GB512" class="GB512_bg">
                <div class="GB512_bg_text">
                    <h1 style="font-size: 15px;">512GB SSD storage</h1>
                </div>
                <div class="GB512_bg_price">
                    <h1 id="GB512_price" style="font-size: 15px;"></h1>
                </div> 
            </div>
            <div onclick="TB1(200); submitForm('', '1TB SSD storage','',totalPriceMacSubTotal, totalPriceMac)" id="TB1" class="TB1_bg">
                <div class="TB1_bg_text">
                    <h1 style="font-size: 15px;">1TB SSD storage</h1>
                </div>
                <div class="TB1_bg_price">
                    <h1 id="TB1_price" style="font-size: 15px;">+ $200.00</h1>
                </div>
            </div>
            <img style="width: 85%; margin-bottom: 0px;" src="images/break_line.png" class="break_line">
            <nav class="total">
                <h1 style="font-size: 30px;" >Total :&nbsp;</h1>
                <h1 id="total" style="font-size: 30px;" >$1,499.00</h1>
            </nav>
            <nav class="ship">
                <form id="shipForm" action="ship.php" method="post">
                    <!-- Hidden input fields to store selected memory and storage options -->
                    <input type="hidden" name="memory" id="memory">
                    <input type="hidden" name="storage" id="storage">
                    <input type="hidden" name="serialNumber" id="serialNumber">
                    <input type="hidden" name="model" id="model" value="MacBook Air">
                    <input type="hidden" name="subTotal" id="subTotal">
                    <input type="hidden" name="totalShip" id="totalShip">
                    <input type="image" style="width: 10%; border-radius: 40px; margin-right: 20px; margin-bottom: 50px; float: right;" src="images/cart.png" alt="Submit">
                </form>
            </nav>
        </div>
    </div>
    <div class="end">
        <center>
            <h3 style="padding-top: 20px;" >Preparing by Group 1</h3>
            <div style="padding-top: 20px;" >Srun kheangsreng, Chhun David, Taing Pa, Khat Tichhung</div>
            <div class="line"></div>
            <h5 style="padding-bottom: 20px;" >Taught by Mr. Chim Bunthoeurn</h5> 
        </center>
    </div>
    <!-- body --> 

    <!-- java -->
    <script src="apple.js"></script>
    <!-- java -->
</body>

</html>